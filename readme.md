This repo has files that show examples of how to:
	1. name the different network interfaces
	2. setup different DHCP zones for these named interfaces
	3. setup firewall zones/rules for the interfaces
	4. enable routing of traffic across these interfaces

Please read the attached readme_routing.doc file for more.